// tipos para el estado
export interface IpDataState {
    status: 'IP' | 'RangeIP';
    ip: string;
    ipRangoDesde: string;
    ipRangoHasta: string;
    file: File | null;
}

// tipos para los errores de validación
export interface ValidationErrors {
    ip: string;
    ipRangoDesde: string;
    ipRangoHasta: string;
}