import { Navigate, Route, Routes } from "react-router-dom";
import { LoginPage } from "../pages/LoginPage";
import {HomePage} from "../pages/HomePage.tsx";
import {Criticidades} from "../pages/Criticidades.tsx";
import {VerificarLogin} from "../pages/VerificarLogin.tsx";
import {PrivatePage} from "../pages/PrivatePage.tsx";

export const AppRouter = () => {
    return(
        <Routes>
            <Route path="/*" element={<Navigate to='/login'/>}/>
            <Route path="verificar-login" element={<VerificarLogin/>}/>
            <Route path="login" element={<LoginPage />} />
            
            <Route path="inicio" element={
                <PrivatePage>
                    <HomePage />
                </PrivatePage>
            } />
            <Route path="criticidades" element={
                <PrivatePage>
                    <Criticidades />
                </PrivatePage>
            } />
        </Routes>
    )
}