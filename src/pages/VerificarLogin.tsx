import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Input } from "../components/Input";
import { Button } from "../components/Button";

export const VerificarLogin = () => {
  const [codigo, setCodigo] = useState("");
  const [error, setError] = useState("");
  const navigate = useNavigate();

  const manejarVerificacion = () => {
    if (codigo === "123456") {
      navigate("/inicio");
    } else {
      setError("El código ingresado es incorrecto.");
    }
  };

  const manejoInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const valor = e.target.value;
    if (/^\d*$/.test(valor) && valor.length <= 6) {
      setCodigo(valor);
      setError("");
    }
    if (valor.length > 6) {
      setError("El código no puede exceder los 6 dígitos");
    }
  };

  return (
    <>
      <div className="min-h-screen bg-black flex items-center justify-center">
        <div className="bg-white p-8 rounded-lg shadow-md w-full max-w-md">
          <h2 className="text-xl font-semibold text-gray-800 text-center mb-4">
            Verifique su identidad
          </h2>
          <div>
            <Input
              type="text"
              label="Escriba el código de seis dígitos enviado al correo j........n@gmail.com"
              id="verfication-code"
              placeholder="Ingrese código de 6 dígitos"
              value={codigo}
              onChange={manejoInputChange}
              divStyle={{ marginBottom: 10 }}
              errorMessage={error}
            />
          </div>
          <Button
            onClick={manejarVerificacion}
            className="w-full bg-orange-400 text-white p-2 rounded-lg hover:bg-orange-500 transition-colors"
            name="Verificar Código"
          />
        </div>
      </div>
    </>
  );
};
