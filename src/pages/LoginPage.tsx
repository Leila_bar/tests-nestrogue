import { ChangeEvent, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Button } from "../components/Button";
import {Input} from "../components/Input";
import image from "/assets/images/Nest_Logo_Principal.png"

export const LoginPage = () => {
    const navigate = useNavigate();
    const [correo, setCorreo] = useState("");
    const [contraseña, setContraseña] = useState("");

    const handleChangeCorreo = (correo: ChangeEvent<HTMLInputElement>) => {
        setCorreo(correo.target.value);
    };

    const handleChangeContraseña = (contraseña: ChangeEvent<HTMLInputElement>) => {
        setContraseña(contraseña.target.value);
    };

    const navegarAVerificarLogin = () => {
        if (correo.trim() === "" || contraseña.trim() === "") {
            alert("Por favor, complete todos los campos");
        } else {
            navigate("/verificar-login");
        }
    };

    return (
        <section className="bg-black">
            <div className="flex flex-col items-center justify-center px-6 py-8 mx-auto md:h-screen lg:py-0">
                <img className="w-40 h-40" src={image} alt="logo" />
                <div className="p-6 space-y-2">
                    <h1 className="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">
                        Iniciar Sesión
                    </h1>
                    <form className="space-y-4" action="#">
                        <div className="w-full md:w-64 lg:w-96">
                            <div className="mb-4">
                                <Input placeholder="Correo Electrónico" type="email" value={correo} onChange={handleChangeCorreo} />
                            </div>
                            <div className="mb-4">
                                <Input placeholder="Contraseña" type="password" value={contraseña} onChange={handleChangeContraseña} />
                            </div>

                            <Button name="Iniciar Sesión" color="orange" width="full" onClick={navegarAVerificarLogin} />

                            <p className="text-center mt-4">
                                <a href="#" className="text-white hover:underline">¿Olvidaste tu contraseña? </a>
                            </p>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    );
};
