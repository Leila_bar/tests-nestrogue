import React, { useCallback } from "react";
import { Input } from "../components/Input.tsx";
import { useIPForm } from "../hooks/useIPForm.ts";
import { useNavigate } from "react-router-dom";
import {Button} from "../components/Button.tsx";

/**
 * Este componente maneja un formulario de entrada de direcciones IP.
 * Permite al usuario seleccionar entre ingresar una única dirección IP o un rango de direcciones IP.
 * También permite al usuario cargar un archivo .csv.
 */
export const HomePage = () => {
    const { ip, ipRangoDesde, ipRangoHasta, status, file, validationErrors, resetForm, setIpData, setValidationErrors } = useIPForm();
    const navigate = useNavigate();

    const validateIP = (ip: string): boolean => {
        const pattern = /^(\d{1,3}\.){3}\d{1,3}(\/\d{1,2})?$/;
        return pattern.test(ip);
    };
    /**
     * Función para manejar los cambios en los campos de entrada del formulario
     */
    const handleInputChange = useCallback((e: React.ChangeEvent<HTMLInputElement>) => {
        const { id, value } = e.target;
        setIpData(prevState => ({ ...prevState, [id]: value }));

        if (id === 'ip' || id === 'ipRangoDesde' || id === 'ipRangoHasta') {
            setValidationErrors(prevState => ({
                ...prevState,
                [id]: validateIP(value) ? '' : 'Por favor, introduce una dirección IP válida.'
            }));
        }
    }, [setIpData, setValidationErrors]);

    /**
     * Función para manejar la selección de un archivo
     */
    const handleFileChange = useCallback((e: React.ChangeEvent<HTMLInputElement>) => {
        const file = e.target.files?.[0];
        if (file) {
            setIpData(prevState => ({ ...prevState, file }));
        }
    }, [setIpData]);

    /**
     * Función para manejar el evento de arrastrar y soltar un archivo
     */
    const handleDrop = useCallback((e: React.DragEvent<HTMLDivElement>) => {
        e.preventDefault();
        if (e.dataTransfer.items) {
            for (let i = 0; i < e.dataTransfer.items.length; i++) {
                if (e.dataTransfer.items[i].kind === 'file') {
                    const file = e.dataTransfer.items[i].getAsFile();
                    if (file && file.type === 'text/csv') {
                        setIpData(prevState => ({ ...prevState, file }));
                    } else {
                        alert('Por favor, selecciona un archivo .csv.');
                    }
                }
            }
        }
    }, [setIpData]);

    /**
     * Función para manejar el evento de arrastrar sobre el área de arrastrar y soltar
     */
    const handleDragOver = useCallback((e: React.DragEvent<HTMLDivElement>) => {
        e.preventDefault();
    }, []);

    const goToCriticidades = () => {
        navigate('/criticidades');
    }

    return (
        <div className="flex justify-center items-center bg-black" style={{ height: 'calc(100vh - 73px)' }}>
            <div className="w-full lg:max-w-screen-lg max-w-screen-sm p-5 bg-gray-50 shadow-md rounded-md">
                <fieldset className="border p-4 rounded-md">
                    <legend className="text-lg font-medium text-gray-700">Seleccionar IP</legend>

                    <div className="flex items-center space-x-4 my-2">
                        <input id="status" className="form-radio" type="radio" name="status" value="IP"
                               checked={status === 'IP'} onChange={handleInputChange}/>
                        <label htmlFor="status" className="text-gray-700">IP</label>
                    </div>

                    <div className="flex items-center space-x-4 my-2">
                        <input id="status" className="form-radio text-blue-500" type="radio" name="status"
                               value="RangeIP"
                               checked={status === 'RangeIP'} onChange={handleInputChange}/>
                        <label htmlFor="status" className="text-gray-700">Rango de IP</label>
                    </div>

                    {status === 'IP' && (
                        <Input
                            id="ip"
                            label="IP"
                            placeholder="xxx.xxx.xxx/nn"
                            value={ip}
                            onChange={handleInputChange}
                            errorMessage={validationErrors.ip}
                        />
                    )}
                    {status === 'RangeIP' && (
                        <div className="flex items-center space-x-2 mt-1">
                            <div className="w-1/2">
                                <Input
                                    id="ipRangoDesde"
                                    label="IP Desde"
                                    placeholder="xxx.xxx.xxx/nn"
                                    value={ipRangoDesde}
                                    onChange={handleInputChange}
                                    errorMessage={validationErrors.ipRangoDesde}
                                />
                            </div>
                            <span className="mx-auto mt-6">A:</span>
                            <div className="w-1/2">
                                <Input
                                    id="ipRangoHasta"
                                    label="IP Hasta"
                                    placeholder="xxx.xxx.xxx/nn"
                                    value={ipRangoHasta}
                                    onChange={handleInputChange}
                                    errorMessage={validationErrors.ipRangoHasta}
                                />
                            </div>
                        </div>
                    )}

                    <div
                        onDrop={handleDrop}
                        onDragOver={handleDragOver}
                        className="mt-1 flex w-full h-40 border-dashed border-2 content-center items-center justify-center rounded-md"
                    >
                        <input type="file" id="file" onChange={handleFileChange} accept=".csv" className="hidden"/>
                        <label htmlFor="file" className="cursor-pointer">
                            {file ? file.name : 'Seleccionar archivo o arrastrar y soltar'}
                        </label>
                    </div>
                    <Button onClick={goToCriticidades} color="orange" className="mt-4 font-bold py-2 px-4 rounded" name="Confirmar IP" disabled={!!Object.values(validationErrors).find(error => error)}/>
                    <Button onClick={resetForm} color="gray" className="mt-4 font-bold py-2 px-4 ml-2 rounded" name="Limpiar"/>
                </fieldset>
            </div>
        </div>
    );
};
