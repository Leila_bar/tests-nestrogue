import { useEffect, useState } from "react"
import { BotonCriticidad, CriticidadType } from "../components/BotonCriticidad"
import { Container } from "../components/Container"
import { DispositivosEncontrados } from "../components/DispositivosEncontrados.tsx";
import { ListaDispositivos } from "../components/ListaDispositivos.tsx";
import { DispositivoData, ObtenerDispositivosService } from "./services/CriticidadesServices.ts";
import { ListaVulnerabilidades } from "../components/ListaVulnerabilidades.tsx";


export const Criticidades = () => {

    const [botonSeleccionado, setBotonSeleccionado] = useState<CriticidadType | undefined>(undefined)
    const [dispositivoSeleccionado, setDispositivoSeleccionado] = useState<DispositivoData | undefined>(undefined)
    const [dispositivos, setDispositivos] = useState<DispositivoData[]>([])

    useEffect(() => {
        if (botonSeleccionado) {
            const data = ObtenerDispositivosService(botonSeleccionado)
            setDispositivos(data)
        }
    }, [botonSeleccionado])


    return (
        <div className="px-10 py-5">
            <DispositivosEncontrados />
            <Container className="flex flex-row gap-10 px-10">
                <BotonCriticidad tipo="error" cantidadRiesgos={12}
                    cantDispositivos={6} activo={botonSeleccionado === "error"} onClick={setBotonSeleccionado} />

                <BotonCriticidad tipo="warning" cantidadRiesgos={4}
                    cantDispositivos={1} activo={botonSeleccionado === "warning"} onClick={setBotonSeleccionado} />

                <BotonCriticidad tipo="info" cantidadRiesgos={7}
                    cantDispositivos={3} activo={botonSeleccionado === "info"} onClick={setBotonSeleccionado} />

            </Container>
            {
                botonSeleccionado &&
                <ListaDispositivos botonSeleccionado={botonSeleccionado} lista={dispositivos}
                    idDispositivoSeleccionado={dispositivoSeleccionado?.ip}
                    onSelectDispositivo={setDispositivoSeleccionado}
                />
            }
            {
                dispositivoSeleccionado &&
                <ListaVulnerabilidades dispositivo={dispositivoSeleccionado} onCloseModal={() => setDispositivoSeleccionado(undefined)} />
            }
        </div>
    )
}