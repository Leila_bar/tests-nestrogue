

export type CriticidadType = 'error'| 'warning' | 'info';
 
interface Props{
    tipo: CriticidadType
    cantidadRiesgos: number,
    cantDispositivos: number,
    activo: boolean,
    style?: React.CSSProperties | undefined,
    className?: string,
    onClick:(tipo:CriticidadType)=> void,
}
export const BotonCriticidad = ( {cantidadRiesgos, cantDispositivos, tipo,  style, onClick, activo}:Props) => {

  const textoBoton = ()=>{
    switch (tipo) {
      case "error":
        return ("Riesgos Criticos");
      case "warning":
        return ("Advertencias");
      case "info":
        return ("Informativos");
    }
  }


  const dispositivos = ()=>{
    let colorTexto: string;
    switch (tipo) {
      case "error":
        colorTexto= "text-error";
        break;
      case "warning":
        colorTexto= "text-warning";
        break;
      case "info":
        colorTexto= "text-info";
        break;
    }
    return <p className={colorTexto+` font-bold select-none`}> {cantDispositivos} dispositivos</p>
  }
  
  return (

    <div className={"cursor-pointer flex flex-1 flex-col rounded-lg bg-slate-0 pt-6 hover:bg-slate-300/50 border-2 "+(activo?(`border-${tipo}`): "border-transparent")} 
    style= {style} onClick={()=> onClick(tipo)}>
        <div className={`border-b-4  border-${tipo} rounded-lg`}>
            <div className={`mx-auto w-16 h-16 rounded-full flex items-center justify-center bg-${tipo} border-4 border-white text-white text-4xl font-bold select-none`}>
            
                {cantidadRiesgos}
                
            </div>
            <div className="text-gray-700 text-2xl font-bold text-center mb-1 select-none">
              {textoBoton()}

            </div>
        </div>
        <div className="text-center bg-[#FFFFFF80] rounded-b-lg p-1">
          {dispositivos()}
        </div>
    </div>    
  )
}