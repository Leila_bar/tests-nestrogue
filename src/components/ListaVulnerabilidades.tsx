import { useEffect, useState } from "react";
import { Vulnerabilidad } from "../interfaces/Vulnerabilidad";
import { DispositivoData } from "../pages/services/CriticidadesServices";
import { Button } from "./Button";
import { Container } from "./Container";
import { TablaVulnerabilidad } from "./TablaVulnerabilidad";
import { obtenerVulnerabilidades } from "../pages/services/VulnerabilidadesServices";

interface Props {
    dispositivo: DispositivoData;
    onCloseModal: () => void
}

export const ListaVulnerabilidades = ({
    dispositivo,
    onCloseModal
}: Props) => {
    const [paginaActual, setPaginaActual] = useState(1);
    const [datosPaginados, setDatosPaginados] = useState<Vulnerabilidad[]>([]);
    const [listaVulnerabilidades, setListaVulnerabilidades] = useState<Vulnerabilidad[]>([]);
    const [totalPaginas, setTotalPaginas] = useState(0);
    const elementosPorPagina = 1;

    useEffect(() => {
        setPaginaActual(1);
        const data = obtenerVulnerabilidades(dispositivo.ip)
        if (data) {
            setListaVulnerabilidades(data)
        }
    }, [dispositivo]);

    useEffect(() => {
        const datos = listaVulnerabilidades.slice(
            (paginaActual - 1) * elementosPorPagina,
            paginaActual * elementosPorPagina
        );
        setDatosPaginados(datos);
    }, [listaVulnerabilidades, paginaActual]);

    useEffect(() => {
        const totalPags = Math.ceil(listaVulnerabilidades.length / elementosPorPagina);
        setTotalPaginas(totalPags);
    }, [listaVulnerabilidades]);

    const paginaAnterior = () => {
        if (paginaActual > 1) {
            setPaginaActual(paginaActual - 1);
        }
    };

    const paginaSiguiente = () => {
        if (paginaActual < totalPaginas) {
            setPaginaActual(paginaActual + 1);
        }
    };

    return (
        <div className="fixed z-50 left-0 top-0 w-full h-full overflow-auto bg-black/40 ">
            <div className="mt-16 m-auto w-[80%] rounded-3xl bg-white border-gray-300 border-2 p-3">
                <Container className="justify-items-center bg-white">
                    {datosPaginados.map(dato => (
                        <TablaVulnerabilidad
                            {...dato}
                        />
                    ))}
                </Container>
                <div className="flex justify-between mt-4 p-4">
                    <div>
                        <Button
                            name="Cerrar"
                            onClick={onCloseModal}
                        />
                    </div>
                    <div className="flex justify-end">
                        <Button
                            onClick={paginaAnterior}
                            disabled={paginaActual === 1}
                            name="Anterior" />
                        <span className="mx-2">
                            Pág. {paginaActual} de {totalPaginas}
                        </span>
                        <Button
                            onClick={paginaSiguiente}
                            disabled={paginaActual === totalPaginas}
                            name="Siguiente" />
                    </div>
                </div>
            </div>
        </div>
    );
};
