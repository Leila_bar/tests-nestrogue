interface Props{
  children: JSX.Element | JSX.Element[],
  className?: string,
  style?: React.CSSProperties | undefined
}

export const Container = ({children, className='', style}:Props) => {


  return (
    <div className={"bg-sky-100 rounded-3xl p-4 "+className}
      style={style}  >
        {children}
    </div>
  )
}
