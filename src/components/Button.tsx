import React from "react";

interface Props extends React.ButtonHTMLAttributes<HTMLButtonElement>{
    width?: 'auto' | 'full';
    color?: 'gray' | 'orange';
}

// Dos posibles valores para el width del botón, auto y full, por defecto es auto.
const widthMap = {
    auto: 'w-auto',
    full: 'w-full'
};

// Dos posibles valores para el color del botón, gray y orange, por defecto es gray.
const colorMap = {
    gray: 'bg-gray-800 hover:bg-gray-900 disabled:bg-gray-400',
    orange: 'bg-orange-400 hover:bg-orange-500 disabled:bg-orange-300'
};

export const Button: React.FC<Props> = (props) => {

    const {width = 'auto', onClick, color = 'gray', name, className} = props;
    const widthClass = widthMap[width];
    const colorClass = colorMap[color];

    return (
        <button
            {...props}
            className={`text-white focus:outline-none focus:ring-1 me-2 focus:ring-gray-300 font-medium rounded-md text-sm px-5 py-2 dark:focus:ring-gray-700 dark:border-gray-700" ${widthClass} ${colorClass} ${className}`}
            onClick={onClick}>
            {name}
        </button>
    );
}