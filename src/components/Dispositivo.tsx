import React from 'react';
import { IconPC } from "./dispositivos-svg/IconPC";
import { IconRO } from "./dispositivos-svg/IconRO";
import { IconSB } from "./dispositivos-svg/IconSB";
import { IconSG } from "./dispositivos-svg/IconSG";
import { IconSW } from "./dispositivos-svg/IconSW";

interface Props {
  /** Define el icono a mostrar */
  tipo: 'server' | 'server-web' | 'server-bd' | 'router' | 'pc';
  /** Nombre del dispositivo */
  nombre: string;
  ip: string;
  /** Color de acuerdo al tipo de criticidad */
  colorPrincipal: 'error' | 'warning' | 'info';
  /** Numero de errores */
  error?: number;
  /** Numero de Advertencias */
  warning?: number;
  /** Numero de Información */
  info?: number;
  activo?: boolean;
  onClick?: ()=>void
}

const iconMap = {
  'server': IconSG,
  'server-web': IconSW,
  'server-bd': IconSB,
  'router': IconRO,
  'pc': IconPC
};

const colors ={
  'error': '#DC3545',
  'warning': '#FFC107',
  'info': '#28A745'
}

/**
 * Información sobre dispositivo de red o servidor con su respectivo estado de criticidad y cantidad de errores, advertencias e información. 
 */
export const Dispositivo: React.FC<Props> = ({tipo, nombre, ip, colorPrincipal,
  error = 0, warning = 0, info = 0, activo, onClick}) => {
  const IconComponent = iconMap[tipo];

  const total = error + warning + info;
  const gradientParts: string[] = [];
  let lastPercentage = 0;

  const addColorToGradient = (color: string, value: number) => {
    if (value <= 0 || total === 0) return;

    const valuePercentage = (value / total) * 100;
    const startPercentage = lastPercentage;
    const endPercentage = lastPercentage + valuePercentage;

    const overlapPercentage = -20 * (value / total); 
    const adjustedStart = startPercentage - overlapPercentage > 0 ? startPercentage - overlapPercentage : startPercentage;
    const adjustedEnd = endPercentage + overlapPercentage < 100 ? endPercentage + overlapPercentage : endPercentage;

    gradientParts.push(`${color} ${adjustedStart.toFixed(2)}%`);
    gradientParts.push(`${color} ${adjustedEnd.toFixed(2)}%`);

    lastPercentage = endPercentage; 
  };

  addColorToGradient(colors.error, error);
  addColorToGradient(colors.warning, warning);
  addColorToGradient(colors.info, info);

  if (gradientParts.length > 0 && lastPercentage < 100) {
    gradientParts.push(`${gradientParts[gradientParts.length - 1].split(' ')[0]} 100%`);
  }

  const gradientStyle = `linear-gradient(to bottom, ${gradientParts.join(', ')})`;

  return (
    <article onClick={onClick} className={` w-56 bg-${colorPrincipal} p-1.5 rounded-2xl transition hover:scale-105 hover:cursor-pointer ${activo? "brightness-110 contrast-125 scale-[1.02]": ""}`}  style={{ backgroundImage: gradientStyle }}  >
      
      <div className="flex flex-col gap-2 items-center bg-[#F1F6FD]/95 border-2 border-white rounded-xl py-6 shadow-lg shadow-zinc-800/30">
        <div className='relative flex flex-col items-center justify-end'>
          <span aria-disabled="true" className='border-error border-warning border-info hidden'></span>
          {IconComponent ? <IconComponent width={160}/> : null}
          <span className={`absolute -bottom-1 border-4 text-neutral-600 border-${colorPrincipal} font-bold py-0.5 px-1 rounded-lg text-xs bg-zinc-50 `}>{ip}</span>
        </div>
        <h2 className='font-bold text-lg text-zinc-800 mt-2'>{nombre}</h2>
        <ul className="flex justify-around gap-4">
          { error > 0 && 
            <li className={`flex justify-center items-center text-white font-black w-8 h-8 rounded-full border-4 border-white p-4 shadow bg-error`}>{error}</li> 
          }
          { warning > 0 && 
            <li className={`flex justify-center items-center text-white font-black w-8 h-8 rounded-full border-4 border-white p-4 shadow bg-warning`}>{warning}</li> 
          }
          { info > 0 && 
            <li className={`flex justify-center items-center text-white font-black w-8 h-8 rounded-full border-4 border-white p-4 shadow bg-info`}>{info}</li> 
          }
        </ul>
      </div>

    </article>
  );
};
