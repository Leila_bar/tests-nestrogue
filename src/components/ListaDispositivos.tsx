import { useEffect, useState } from 'react';
import { CriticidadType } from './BotonCriticidad.tsx';
import { Dispositivo } from './Dispositivo';
import {Container} from "./Container.tsx";
import {Button} from "./Button.tsx";
import {DispositivoData} from '../pages/services/CriticidadesServices.ts';

interface Props {
    botonSeleccionado: CriticidadType;
    lista: DispositivoData[];
    idDispositivoSeleccionado?: string;
    onSelectDispositivo?: (dispositivo: DispositivoData|undefined)=>void
}

export const ListaDispositivos: React.FC<Props> = ({botonSeleccionado, lista,
    idDispositivoSeleccionado, onSelectDispositivo=()=>{}}) => {
    const [paginaActual, setPaginaActual] = useState(1);
    const [datosPaginados, setDatosPaginados] = useState<DispositivoData[]>([])
    const [totalPaginas, setTotalPaginas] = useState(0)
    const elementosPorPagina = 8;

    useEffect(() => {
        setPaginaActual(1);
        onSelectDispositivo(undefined)
    }, [botonSeleccionado, onSelectDispositivo]);

    useEffect(() => {
        const datos = lista.slice((paginaActual - 1) * elementosPorPagina, paginaActual * elementosPorPagina);
        setDatosPaginados(datos)
    }, [lista, paginaActual])
    
    useEffect(() => {
        const totalPags = Math.ceil(lista.length / elementosPorPagina);
        setTotalPaginas(totalPags)
    }, [lista])
    

    const paginaAnterior = () => {
        if (paginaActual > 1) {
            setPaginaActual(paginaActual - 1);
        }
    };

    const paginaSiguiente = () => {
        if (paginaActual < totalPaginas) {
            setPaginaActual(paginaActual + 1);
        }
    };

    const onDispositivoClicked = (dispositivo: DispositivoData) => {
        if(dispositivo.ip !== idDispositivoSeleccionado)
            onSelectDispositivo(dispositivo)
        else
            onSelectDispositivo(undefined)
    }

    return (
        <div className="flex flex-col mt-5">
            <Container className="grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-3 justify-items-center">
                {datosPaginados.map(dato => (
                    <Dispositivo
                        tipo={dato.tipo}
                        nombre={dato.nombre}
                        ip={dato.ip}
                        colorPrincipal={botonSeleccionado}
                        error={dato.error}
                        warning={dato.warning}
                        info={dato.info}
                        activo={idDispositivoSeleccionado === dato.ip}
                        onClick={()=>onDispositivoClicked(dato)}
                    />
                ))}
            </Container>
            <div className="flex justify-end mt-4">
                <Button onClick={paginaAnterior} disabled={paginaActual === 1} name="Anterior">Anterior</Button>
                <span className="mx-2">Pág. {paginaActual} de {totalPaginas}</span>
                <Button onClick={paginaSiguiente} disabled={paginaActual === totalPaginas} name="Siguiente">Siguiente</Button>
            </div>
        </div>
    );
};