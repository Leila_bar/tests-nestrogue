import { useNavigate } from 'react-router-dom'
import imageLogo from "/assets/images/Nest_Logo_Principal.png"
import imagePerfil from "/assets/images/perfil.jpg"



const Header = () => {
  const navigate = useNavigate();

  const cerrarSesion=()=>{
    
    navigate('/login');
    
  }


  return (
    <header className="bg-gray-800 text-white p-4">
      <div className="flex items-center justify-between">
      <img src={imageLogo} className="h-10 w-auto" />
        <nav>

        </nav>
        <button className="ml-auto p-2 text-gray-300 hover:underline" onClick={cerrarSesion}>
            Cerrar sesión
          </button>
        <img src={imagePerfil} className="h-8 w-8 rounded-full" />
      </div>
    </header>
  );
};

export default Header;