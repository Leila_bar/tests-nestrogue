import React from "react";

interface InputProps extends React.InputHTMLAttributes<HTMLInputElement> {
    label?: string;
    errorMessage?: string;
    /**Para el estilo global (div) del componente, no del input - Ej.: Márgenes*/
    divStyle?: React.CSSProperties
}

export const Input: React.FC<InputProps> = (props) => {
    const {label, errorMessage, id, className, divStyle} = props;
    return(
    <div className="mt-1" style={divStyle}>
        <label htmlFor={id} className="block text-sm font-medium text-gray-700">{label}</label>
        <input
            {...props}
            className={`mt-1 block w-full p-2 border ${errorMessage ? 'border-red-500' : 'border-gray-300'} rounded-md `+ className}
        />
        {errorMessage && <p className="text-red-500 text-xs">{errorMessage}</p>}
    </div>
)};
