import { SVGProps } from "react"
const SvgComponent = (props: SVGProps<SVGSVGElement>) => (
  <svg
    width="130"
    fill="#fafafa"
    stroke="#4D4D4D"
    viewBox="0 0 130 120"
    {...props}
  >
    <path
      stroke={props.stroke}
      strokeMiterlimit={10}
      strokeWidth={3}
      d="M38.456 56.324v11.493M32.088 56.324v11.493M99.354 56.424v11.493M92.99 56.424v11.493M38.456 25.217v11.494M32.088 25.217v11.494M99.354 25.317v11.494M92.99 25.317v11.494"
    />
    <path
      fill={props.fill}
      stroke={props.stroke}
      strokeMiterlimit={10}
      strokeWidth={3}
      d="M103.473 66.201h-75.5a3.632 3.632 0 0 0-3.632 3.632v14.934a3.632 3.632 0 0 0 3.631 3.632h75.501a3.632 3.632 0 0 0 3.632-3.632V69.833a3.632 3.632 0 0 0-3.632-3.632Z"
    />
    <path
      fill={props.fill}
      stroke={props.stroke}
      strokeMiterlimit={10}
      strokeWidth={3}
      d="M103.473 66.201H88.538a3.632 3.632 0 0 0-3.632 3.632v14.934a3.632 3.632 0 0 0 3.632 3.632h14.935a3.632 3.632 0 0 0 3.632-3.632V69.833a3.632 3.632 0 0 0-3.632-3.632Z"
    />
    <path
      stroke={props.stroke}
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth={3}
      d="M31.922 77.29v4.063M38.126 77.29v4.063M44.327 77.29v4.063M50.528 77.29v4.063M76.905 77.298H62.112"
    />
    <path
      fill={props.fill}
      stroke={props.stroke}
      strokeMiterlimit={10}
      strokeWidth={3}
      d="M96.004 80.721a3.423 3.423 0 1 0 0-6.845 3.423 3.423 0 0 0 0 6.845ZM103.473 35.337h-75.5a3.632 3.632 0 0 0-3.632 3.631v14.935a3.632 3.632 0 0 0 3.631 3.631h75.501a3.632 3.632 0 0 0 3.632-3.631V38.968a3.632 3.632 0 0 0-3.632-3.631Z"
    />
    <path
      fill={props.fill}
      stroke={props.stroke}
      strokeMiterlimit={10}
      strokeWidth={3}
      d="M103.473 35.337H88.538a3.632 3.632 0 0 0-3.632 3.631v14.935a3.632 3.632 0 0 0 3.632 3.631h14.935a3.632 3.632 0 0 0 3.632-3.631V38.968a3.632 3.632 0 0 0-3.632-3.631Z"
    />
    <path
      stroke={props.stroke}
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth={3}
      d="M31.922 46.425v4.064M38.126 46.425v4.064M44.327 46.425v4.064M50.528 46.425v4.064M76.905 46.434H62.112"
    />
    <path
      fill={props.fill}
      stroke={props.stroke}
      strokeMiterlimit={10}
      strokeWidth={3}
      d="M96.004 49.857a3.423 3.423 0 1 0 0-6.846 3.423 3.423 0 0 0 0 6.846ZM103.473 4.473h-75.5a3.632 3.632 0 0 0-3.632 3.631v14.934a3.631 3.631 0 0 0 3.631 3.632h75.501a3.632 3.632 0 0 0 3.632-3.632V8.104a3.632 3.632 0 0 0-3.632-3.631Z"
    />
    <path
      stroke={props.stroke}
      strokeMiterlimit={10}
      strokeWidth={3}
      d="M103.473 4.473H88.538a3.632 3.632 0 0 0-3.632 3.631v14.934a3.631 3.631 0 0 0 3.632 3.632h14.935a3.632 3.632 0 0 0 3.632-3.632V8.104a3.632 3.632 0 0 0-3.632-3.631Z"
    />
    <path
      stroke={props.stroke}
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth={3}
      d="M31.922 15.56v4.068M38.126 15.56v4.068M44.327 15.56v4.068M50.528 15.56v4.068M76.905 15.573H62.112"
    />
    <path
      stroke="#4D4D4D"
      strokeMiterlimit={10}
      strokeWidth={3}
      d="M95.254 18.995a3.423 3.423 0 1 0 0-6.845 3.423 3.423 0 0 0 0 6.845ZM68.906 90v16M61.094 90v16M7.589 117.178a5.589 5.589 0 1 0 0-11.178 5.589 5.589 0 0 0 0 11.178ZM11.178 107.683H66M11.178 115.495H66M64 107.683h54.822M64 115.495h54.822M122.411 117.178a5.589 5.589 0 1 0 0-11.177 5.589 5.589 0 0 0 0 11.177Z"
    />
  </svg>
)
export { SvgComponent as IconSG }
