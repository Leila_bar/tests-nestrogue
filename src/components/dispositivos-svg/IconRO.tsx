import { SVGProps } from "react"
const SvgComponent = (props: SVGProps<SVGSVGElement>) => (
  <svg
    width="130"
    fill="#fafafa"
    stroke="#4D4D4D"
    viewBox="0 0 130 120"
    {...props}
  >
    <path
      fill={props.fill}
      d="M34.16 61.678V7.801a3.906 3.906 0 0 0-7.812 0v53.877"
    />
    <path
      stroke={props.stroke}
      strokeMiterlimit={10}
      strokeWidth={3}
      d="M34.16 61.678V7.801a3.906 3.906 0 0 0-7.812 0v53.877M26.348 13.552h7.812M26.348 50.758h7.812"
    />
    <path
      fill={props.fill}
      d="M104.588 61.678V7.801a3.906 3.906 0 0 0-7.812 0v53.877"
    />
    <path
      stroke={props.stroke}
      strokeMiterlimit={10}
      strokeWidth={3}
      d="M104.588 61.678V7.801a3.906 3.906 0 0 0-7.812 0v53.877M96.776 13.552h7.812M96.776 50.758h7.812"
    />
    <path
      fill={props.fill}
      stroke={props.stroke}
      strokeMiterlimit={10}
      strokeWidth={3}
      d="M65.468 49.753a3.393 3.393 0 1 0 0-6.785 3.393 3.393 0 0 0 0 6.785ZM86.195 25.633l-3.105 3.105a1.2 1.2 0 0 1-1.659.033 23.76 23.76 0 0 0-15.966-6.165 23.737 23.737 0 0 0-15.963 6.165 1.203 1.203 0 0 1-1.659-.033l-3.105-3.105a1.205 1.205 0 0 1 .03-1.73 30.559 30.559 0 0 1 20.697-8.086c7.68 0 15.072 2.898 20.7 8.085a1.2 1.2 0 0 1 .03 1.731ZM76.583 35.245l-3.114 3.114a1.195 1.195 0 0 1-1.596.087 10.188 10.188 0 0 0-12.807 0 1.195 1.195 0 0 1-1.596-.087l-3.114-3.114a1.206 1.206 0 0 1 .072-1.767 16.987 16.987 0 0 1 11.043-4.086 16.98 16.98 0 0 1 11.046 4.086c.534.456.567 1.27.072 1.767h-.006ZM103.787 88.483H27.149a3.6 3.6 0 0 1-3.456-2.595l-2.547-8.757h88.644l-2.547 8.757a3.6 3.6 0 0 1-3.456 2.595Z"
    />
    <path
      fill={props.fill}
      stroke={props.stroke}
      strokeMiterlimit={10}
      strokeWidth={3}
      d="M108.89 77.131H22.046a3.599 3.599 0 0 1-3.6-3.6v-4.146c0-3.975 3.225-7.2 7.2-7.2h79.644c3.975 0 7.2 3.225 7.2 7.2v4.146c0 1.99-1.611 3.6-3.6 3.6Z"
    />
    <path
      stroke={props.stroke}
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth={3}
      d="M30.146 84.586v3.897M36.959 84.586v3.897M43.769 84.586v3.897M87.167 84.586v3.897M93.977 84.586v3.897M100.787 84.586v3.897M29.852 69.664h-3.897M40.56 69.664h-3.898M51.27 69.664h-3.898M60.491 69.664h-.927"
    />
    <path
      fill={props.fill}
      stroke={props.stroke}
      strokeMiterlimit={10}
      strokeWidth={3}
      d="M81.5 62.278h20.181v4.461a2.4 2.4 0 0 1-2.4 2.4H83.9a2.4 2.4 0 0 1-2.4-2.4v-4.46Z"
    />
    <path
      stroke={props.stroke}
      strokeMiterlimit={10}
      strokeWidth={3}
      d="M68.906 90v16M61.094 90v16M7.589 117.178a5.589 5.589 0 1 0 0-11.178 5.589 5.589 0 0 0 0 11.178ZM11.178 107.683H66M11.178 115.495H66M64 107.683h54.822M64 115.495h54.822M122.411 117.178a5.589 5.589 0 1 0 0-11.177 5.589 5.589 0 0 0 0 11.177Z"
    />
  </svg>
)
export { SvgComponent as IconRO }
