import { SVGProps } from "react"
const SvgComponent = (props: SVGProps<SVGSVGElement>) => (
  <svg
    width="130"
    fill="#fafafa"
    stroke="#4D4D4D"
    viewBox="0 0 130 120"
    {...props}
  >
    <g clipPath="url(#a)">
      <path
        fill={props.fill}
        stroke={props.stroke}
        strokeMiterlimit={10}
        strokeWidth={3}
        d="M113.867 1.453H16.511c-3.976 0-7.2 3.023-7.2 6.75v55.75c0 3.728 3.224 6.75 7.2 6.75h97.356c3.976 0 7.2-3.022 7.2-6.75V8.203c0-3.727-3.224-6.75-7.2-6.75Z"
      />
      <path
        stroke={props.stroke}
        strokeMiterlimit={10}
        strokeWidth={3}
        d="M16.793 62.018v-50.36c0-1.865 1.611-3.376 3.6-3.376h89.595c1.989 0 3.6 1.51 3.6 3.375v50.36"
      />
      <path
        fill={props.fill}
        stroke={props.stroke}
        strokeMiterlimit={10}
        strokeWidth={3}
        d="M76.819 70.703h-23.26c-.662 0-1.2.504-1.2 1.125v9.253c0 .621.538 1.125 1.2 1.125h23.26c.662 0 1.2-.504 1.2-1.125v-9.253c0-.621-.538-1.125-1.2-1.125Z"
      />
      <path
        stroke={props.stroke}
        strokeMiterlimit={10}
        strokeWidth={3}
        d="M9.548 62.018H120.72"
      />
      <path
        fill={props.fill}
        stroke={props.stroke}
        strokeMiterlimit={10}
        strokeWidth={3}
        d="M89.795 82.206H40.541c-.663 0-1.2.504-1.2 1.125v4.005c0 .621.537 1.125 1.2 1.125h49.254c.663 0 1.2-.504 1.2-1.125V83.33c0-.621-.537-1.125-1.2-1.125Z"
      />
      <path
        stroke={props.stroke}
        strokeMiterlimit={10}
        strokeWidth={3}
        d="M67.8 75.023h-5.263c-.663 0-1.2.504-1.2 1.125v4.933c0 .621.537 1.125 1.2 1.125H67.8c.663 0 1.2-.504 1.2-1.125v-4.933c0-.621-.537-1.125-1.2-1.125Z"
      />
    </g>
    <path
      stroke={props.stroke}
      strokeMiterlimit={10}
      strokeWidth={3}
      d="M68.906 90v16M61.094 90v16M7.589 117.178a5.589 5.589 0 1 0 0-11.178 5.589 5.589 0 0 0 0 11.178ZM11.178 107.683H66M11.178 115.495H66M64 107.683h54.822M64 115.495h54.822M122.411 117.178a5.589 5.589 0 1 0 0-11.177 5.589 5.589 0 0 0 0 11.177Z"
    />
    
  </svg>
)
export { SvgComponent as IconPC }
