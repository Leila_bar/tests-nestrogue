import {useState} from "react";
import {IpDataState, ValidationErrors} from "../interfaces/homePage.ts";

export const useIPForm = () => {
    const [IpData, setIpData] = useState<IpDataState>({
        status: 'IP',
        ip: '',
        ipRangoDesde: '',
        ipRangoHasta: '',
        file: null,
    });

    const [validationErrors, setValidationErrors] = useState<ValidationErrors>({
        ip: '',
        ipRangoDesde: '',
        ipRangoHasta: '',
    });

    const resetForm = () => {
        setIpData({
            status: 'IP',
            ip: '',
            ipRangoDesde: '',
            ipRangoHasta: '',
            file: null,
        });
        setValidationErrors({
            ip: '',
            ipRangoDesde: '',
            ipRangoHasta: '',
        });
    };

    return { ...IpData, validationErrors, resetForm, setIpData, setValidationErrors };
};
