/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        error: '#DC3545',
        warning: '#FFC107',
        info: '#28A745',
      },
    },
  },
  plugins: [],
}

